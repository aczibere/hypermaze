﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//<summary>
//Game object, that creates maze and instantiates it in scene
//</summary>
public class MazeSpawner : MonoBehaviour {
	public enum MazeGenerationAlgorithm{
		PureRecursive,
		RecursiveTree,
		RandomTree,
		OldestTree,
		RecursiveDivision,
	}

	public MazeGenerationAlgorithm Algorithm = MazeGenerationAlgorithm.PureRecursive;
	//public bool FullRandom = false;
	//public int RandomSeed = 12345;
	public GameObject Floor = null;
	public GameObject Wall = null;
	public GameObject Pillar = null;
	public int Rows = 5;
	public int Columns = 5;
    public float cellmultiplier = 1;
	float CellWidth = 5;
	float CellHeight = 5;

	public bool AddGaps = true;
	public GameObject GoalPrefab = null;
    public int numOfGoals = 1;

    int goalsGenerated = 0;

	private BasicMazeGenerator mMazeGenerator = null;

    ///////////////////////////////////
    [Header("Custom stuff")]
    public Transform ball;
    public ColorPalette[] colors;
    public GameObject trigger;
    public GameObject shadow;

    //Transform mazeRroot;
    Vector3 farestGoalPos = Vector3.zero;
    [HideInInspector]
    public GameObject firstWall;
    public int level = 0;

    public float GetAvatarPosX()
    {
        var sr_wall = Wall.GetComponent<SpriteRenderer>().sprite;
        var cellsize = sr_wall.bounds.size.y * Wall.transform.localScale.y * cellmultiplier;

        var x = 0 - cellsize * (((float)Rows / 2f) - 0.5f);
        return x;
    }
  
    public void ModifyNumOfgoals(int delta)
    {
        numOfGoals = Mathf.Max(1, numOfGoals + delta);
    }

    public GameObject GeneRateNew(Vector3 endPos)
    {
        ColorPalette cp = colors[Random.Range(0, colors.Length )];
        goalsGenerated = 0;
        farestGoalPos = GameController.Instance.BallStartPos;
        var mazeRroot = new GameObject("MazeRoot").transform;

        var sh = Instantiate(shadow);
        sh.transform.SetParent(mazeRroot);

        /*
        if (!FullRandom)
        {
            Random.seed = RandomSeed;
        }
        */
        Debug.Log("level: " + level );
        if (level > 0)
        {
            
            Random.seed = level;
            Debug.Log("random: " + Random.seed);
        }

        switch (Algorithm)
        {
            case MazeGenerationAlgorithm.PureRecursive:
                mMazeGenerator = new RecursiveMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.RecursiveTree:
                mMazeGenerator = new RecursiveTreeMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.RandomTree:
                mMazeGenerator = new RandomTreeMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.OldestTree:
                mMazeGenerator = new OldestTreeMazeGenerator(Rows, Columns);
                break;
            case MazeGenerationAlgorithm.RecursiveDivision:
                mMazeGenerator = new DivisionMazeGenerator(Rows, Columns);
                break;
        }
        mMazeGenerator.GenerateMaze();

        var sr_wall = Wall.GetComponent<SpriteRenderer>().sprite;
        var cellsize = sr_wall.bounds.size.y * Wall.transform.localScale.y * cellmultiplier;
        Debug.Log("cellsize: " + cellsize);
        CellHeight = cellsize;
        CellWidth = cellsize;

        var pos = new Vector3( (cellsize / 2.0f) * Rows - (cellsize*0.5f), (cellsize / 2.0f) * Columns - (cellsize * 0.5f), 0f);
        mazeRroot.position = pos;

        //Trigger
        //var triggerObj = Instantiate(trigger, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        //triggerObj.transform.parent = mazeRroot;

        //Floor
        var floorObj = Instantiate(Floor, mazeRroot.position, Quaternion.identity) as GameObject;
        var paddingScale = 1f;
        floorObj.transform.localScale = new Vector3(cellsize * Rows * sr_wall.pixelsPerUnit * paddingScale, cellsize * Columns *sr_wall.pixelsPerUnit * paddingScale, 1);
        floorObj.GetComponentInChildren<SpriteRenderer>().color = cp.baseColor;
        floorObj.transform.parent = mazeRroot;

        for (int row = 0; row < Rows; row++)
        {
            for (int column = 0; column < Columns; column++)
            {
                float x = column * (CellWidth + (AddGaps ? 0.2f : 0));
                float y = row * (CellHeight + (AddGaps ? 0.2f : 0));
                MazeCell cell = mMazeGenerator.GetMazeCell(row, column);
                GameObject tmp;
                //tmp = Instantiate(Floor, new Vector3(x, 0, z), Quaternion.Euler(0, 0, 0)) as GameObject;
                //tmp.transform.parent = transform;

                //tmp.GetComponentInChildren<Renderer>().material.color = cp.baseColor;

                if (cell.WallRight)
                {
                    tmp = Instantiate(Wall, new Vector3(x + CellWidth / 2, y, 0) + Wall.transform.position, Quaternion.identity) as GameObject;// right
                    tmp.transform.parent = mazeRroot;
                    tmp.GetComponentInChildren<SpriteRenderer>().color = cp.wallColor;
                    tmp.transform.localScale *= cellmultiplier;
                }
                
                if (cell.WallFront)
                {
                    tmp = Instantiate(Wall, new Vector3(x, y + CellHeight / 2, 0) + Wall.transform.position, Quaternion.Euler(0, 0, 90)) as GameObject;// front
                    tmp.transform.parent = mazeRroot;
                    tmp.GetComponentInChildren<SpriteRenderer>().color = cp.wallColor;
                    tmp.transform.localScale *= cellmultiplier;
                }
                if (cell.WallLeft)
                {
                    tmp = Instantiate(Wall, new Vector3(x - CellWidth / 2, y, 0) + Wall.transform.position, Quaternion.identity) as GameObject;// left
                    tmp.transform.parent = mazeRroot;
                    tmp.GetComponentInChildren<SpriteRenderer>().color = cp.wallColor;
                    tmp.transform.localScale *= cellmultiplier;
                }
                if (cell.WallBack)
                {
                    tmp = Instantiate(Wall, new Vector3(x, y - CellHeight / 2, 0) + Wall.transform.position, Quaternion.Euler(0, 0,90)) as GameObject;// back
                    tmp.transform.parent = mazeRroot;
                    tmp.GetComponentInChildren<SpriteRenderer>().color = cp.wallColor;
                    tmp.transform.localScale *= cellmultiplier;

                    if (row == 0 && column == 0)
                    {
                        firstWall = tmp;
                        firstWall.SetActive(false);
                        //Trigger
                        var triggerObj = Instantiate(trigger, new Vector3(x, y - CellHeight / 2, 0) + Wall.transform.position, Quaternion.Euler(0, 0, 90)) as GameObject;
                        triggerObj.transform.parent = mazeRroot;
                        triggerObj.transform.localScale *= cellmultiplier;
                    }

                }
                
                if ( goalsGenerated < numOfGoals && numOfGoals > 0 && cell.IsGoal && GoalPrefab != null)
                {
                    var newPos = new Vector3(x, y, 0);
                    if (numOfGoals == 1)
                    {
                        if (Vector3.Distance(farestGoalPos, GameController.Instance.BallStartPos) < Vector3.Distance(GameController.Instance.BallStartPos, newPos))
                        {
                            farestGoalPos = newPos;
                        }
                    }
                    else
                    {
                        tmp = Instantiate(GoalPrefab, newPos , Quaternion.Euler(0, 0, 0)) as GameObject;
                        tmp.transform.parent = mazeRroot;
                        goalsGenerated++;
                    }
                }
            }
        }

        if (numOfGoals == 1)
        {
            var tmp = Instantiate(GoalPrefab, farestGoalPos, Quaternion.Euler(0, 0, 0)) as GameObject;
            tmp.transform.parent = mazeRroot;
            goalsGenerated++;
        }

        if (Pillar != null)
        {
            for (int row = 0; row < Rows + 1; row++)
            {
                for (int column = 0; column < Columns + 1; column++)
                {
                    float x = column * (CellWidth + (AddGaps ? 0.2f : 0));
                    float z = row * (CellHeight + (AddGaps ? 0.2f : 0));
                    GameObject tmp = Instantiate(Pillar, new Vector3(x - CellWidth / 2, z - CellHeight / 2, 0 ), Quaternion.identity) as GameObject;
                    tmp.transform.parent = mazeRroot;
                    tmp.GetComponentInChildren<SpriteRenderer>().color = cp.wallColor;
                    tmp.transform.localScale *= cellmultiplier;
                }
            }
        }

        mazeRroot.transform.position = endPos;
        return mazeRroot.gameObject;
    }

    public void ModifyLevel(int delta)
    {
        level += delta;
    }

}
