﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LevelsManager
{
    const string KEY_PREFIX = "levelUnlocked_";

    public static void UnlockLevel(int i)
    {
        PlayerPrefs.SetInt(KEY_PREFIX + i, 1);
        PlayerPrefs.Save();
    }

    public static bool IsUnlocked(int i)
    {
        bool val = PlayerPrefs.GetInt(KEY_PREFIX + i, 0) == 1;
        return val;
    }

}
