﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[System.Serializable]
public class Level
{
    [SerializeField]
    public int goals = 1;
    public float speed = 10.0f;

}
