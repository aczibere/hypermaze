﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Maze/Create Difficulty Template")]
public class Difficulty : ScriptableObject
{
    public string difficultyName;
    public int id;
    public int numOfGoals;
    public float avatarSpeed;
    public int startLevelindex;
    public int numOfLevels;
}

