﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;

[System.Serializable]
public class ColorPalette  {

    public Color baseColor;
    public Color wallColor;

}
