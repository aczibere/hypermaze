﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ThemeSetter : MonoBehaviour {

    public string id;

    void Awake()
    {
        var button = GetComponent<Button>();

        button.onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        BckgSpriteHolder.Instance().SetSelectedId(id);
    }
}
