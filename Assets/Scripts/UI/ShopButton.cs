﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopButton : MonoBehaviour {

    public enum State
    {
        Locked,
        Unlocked,
        Selected
    }

    public const string KEY_AVATAR_SELECTED = "AVATAR_SELECTED";
    public const string KEY_AVATAR_UNLOCKED = "AVATAR_UNLOCKED_";
    public int price = 0;
    public TextMeshProUGUI text;
    public GameObject priceRoot;
    public Image image;
    public Image btnImage;

    State state = State.Locked;
    Button btn;
    public ShopUIManager uiManager;


    // Use this for initialization
    public void Init()
    {
        UpdateState();
        btn = GetComponent<Button>();
        btn.onClick.AddListener(OnClick);
        //UpdateUI();
    }

    public string GetID()
    {
        var id = image.sprite.name;
        return id;
    }

    void UpdateState()
    {
        if (PlayerPrefs.GetString(KEY_AVATAR_SELECTED) == GetID())
        {
            state = State.Selected;
        }
        else if (PlayerPrefs.GetInt(KEY_AVATAR_UNLOCKED + GetID()) == 1)
        {
            state = State.Unlocked;
        }
        else
        {
            state = State.Locked;
        }
        //Debug.Log(gameObject.name + "state: " + state);
    }

    public void OnClick()
    {
        Debug.Log("Onclick: " + name + "state: " + state);
        switch (state)
        {
            case State.Locked:
                LockedClick();
                break;
            case State.Unlocked:
                UnlockedClick();
                break;
            case State.Selected:
                break;
            default:
                break;
        }
        uiManager.UpdateUI();
    }

    void LockedClick()
    {
        Debug.Log("LockedClick");
        if (CurrencyManager.Instance.GetCurrency(CurrencyManager.CURRENCY_TYPE_STAR) >= price)
        {
            AnalyticsManager.Instance.LogAvatarBought(price);

            //TODO: play unlock anim
            CurrencyManager.Instance.SpendCurreny(CurrencyManager.CURRENCY_TYPE_STAR, price);
            var key = KEY_AVATAR_UNLOCKED + GetID();
            Debug.Log("key: " + key + " key2: " + KEY_AVATAR_SELECTED);
            PlayerPrefs.SetInt(key, 1);
            PlayerPrefs.SetString(KEY_AVATAR_SELECTED, GetID());
            PlayerPrefs.Save();
        }
        else
        {
            var msg = "You don't have enough stars to buy this :(";
            MessageWindow.Show("OH NO!", msg);
        }
    }

    void UnlockedClick()
    {
        PlayerPrefs.SetString(KEY_AVATAR_SELECTED, GetID());
        PlayerPrefs.Save();
    }

    public void UpdateUI()
    {
        UpdateState();
        switch (state)
        {
            case State.Locked:
                text.text = price + "";
                priceRoot.gameObject.SetActive(true);
                image.gameObject.SetActive(false);
                btnImage.color = uiManager.lockedColor;
                break;
            case State.Unlocked:
                priceRoot.gameObject.SetActive(false);
                image.gameObject.SetActive(true);
                btnImage.color = uiManager.unlockedColor;
                break;
            case State.Selected:
                priceRoot.gameObject.SetActive(false);
                image.gameObject.SetActive(true);
                btnImage.color = uiManager.selectedColor;
                break;
            default:
                break;
        }
    }


	
}
