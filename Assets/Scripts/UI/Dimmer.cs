﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dimmer : MonoBehaviour {

    //public Image img;
    Animator animator;

    bool dimmerActive = false;

    int winCount = 0;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Activate()
    {
        winCount++;
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Base"))
        {
            animator.SetBool("dim", true);
        }
    }

    public void Deactivate()
    {
        winCount--;
        if (winCount == 0 && animator.GetCurrentAnimatorStateInfo(0).IsName("dimmer_dimmed"))
        {
            animator.SetBool("dim", false);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
}
