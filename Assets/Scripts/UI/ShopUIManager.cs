﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ShopUIManager : MonoBehaviour {

    public AudioSource btnSound;
    public TextMeshProUGUI starsText;
    public GameObject buttonTemplate;
    //public Sprite[] sprites;
    public Transform root;
    public ScrollRect scrollRect;

    public Color lockedColor;
    public Color unlockedColor;
    public Color selectedColor;

    ShopButton[] buttons;

    public void GenerateButtons()
    {
        var sprites = AvatarSpriteHolder.Instance().sprites;
        buttons = GetComponentsInChildren<ShopButton>(true);

        if (buttons == null || buttons.Length < sprites.Length)
        {
            buttons = new ShopButton[sprites.Length];
            for (int i = 0; i < sprites.Length; i++)
            {
                var btn = Instantiate(buttonTemplate);
                btn.transform.SetParent(root.transform, false);
                btn.GetComponent<ButtonSound>().clickSound = btnSound;
                var shopButton = btn.GetComponent<ShopButton>();
                shopButton.price = (i + 1) * 2;
                shopButton.image.sprite = sprites[i];
                buttons[i] = shopButton;
                buttons[i].uiManager = this;
                buttons[i].Init();
            }
        }

        UpdateUI();
    }

    public void UpdateUI()
    {
        foreach (var item in buttons)
        {
            item.UpdateUI();
        }
        starsText.text = "" + CurrencyManager.Instance.GetCurrency(CurrencyManager.CURRENCY_TYPE_STAR);
        
    }

    public IEnumerator JumpToStart()
    {
        Debug.Log("ShopUI");
        yield return new WaitForEndOfFrame();
        scrollRect.verticalNormalizedPosition =1f;
    }
	
}
