﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PopupWindow))]
public class PopupEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PopupWindow myScript = (PopupWindow)target;
        if (GUILayout.Button("Show"))
        {
            myScript.Show();
        }
        if (GUILayout.Button("Hide"))
        {
            myScript.Hide();
        }
    }

}
