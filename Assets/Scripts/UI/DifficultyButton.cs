﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyButton : MonoBehaviour {

    public Difficulty difficulty;
    Button button;
    MainMenuUIManager mmUIManager;

    void Awake()
    {
        mmUIManager = FindObjectOfType<MainMenuUIManager>();
        button = GetComponent<Button>();

        button.onClick.AddListener( OnClick);
    }

    public void OnClick()
    {
        StaticMessage.selectedDifficulty = difficulty;
        mmUIManager.OnDifficultySelected();
    }
}
