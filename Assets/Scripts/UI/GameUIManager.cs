﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameUIManager : MonoBehaviour {

    public enum UIBoxType
    {
        Star,
        Gem,
        Speed,
        Stage
    }

    [Header("UI Refernces")]
    public TextMeshProUGUI scoreUI;
    public TextMeshProUGUI gemUI;
    public TextMeshProUGUI stageText;
    public TextMeshProUGUI speedText;
    public GameObject startArrow;

    [Header("Theme")]
    public Image background;
    public MazeSpawner mazeSpawner;

    [Header("Needed for GameController")]
    //public Button startButton;
    public TutorialManager tutManager;
    public RewardedVideoWindow rewVideoWin;
    public ContinueGameWindow contGameWin;

    [Header("GameController references")]
    public GameControllerInfinite GCI;
    public GameControllerLevels GCL;

    [HideInInspector]
    public Animator animatorStarUI;
    [HideInInspector]
    public Animator animatorGemUI;
    [HideInInspector]
    public Animator animatorStageText;
    [HideInInspector]
    public Animator animatorSpeedText;

    int gemCount = 0;
    int starsCount = 0;
    int stage = 0;
    int level = 0;
    float speed = 0f;

    void Awake()
    {
        animatorStarUI = scoreUI.GetComponentInParent<Animator>();
        animatorGemUI = gemUI.GetComponentInParent<Animator>();
        animatorStageText = stageText.GetComponentInParent<Animator>();
        animatorSpeedText = speedText.GetComponentInParent<Animator>();     
    }

    void Start()
    {
        SetTheme();
    }

    void SetTheme()
    {
        var theme = BckgSpriteHolder.Instance().GetSelectedTheme();
        if (theme == null)
            return;

        background.sprite = theme.background;
        mazeSpawner.colors[0].baseColor = theme.baseColor;
        mazeSpawner.colors[0].wallColor = theme.wallColor;
        GameController.Instance.Avatar.ChangeColor(theme.avatarColor);
    }

    public void UpdateUI()
    {
        var newStarCount = CurrencyManager.Instance.GetCurrency(CurrencyManager.CURRENCY_TYPE_STAR);
        starsCount = newStarCount;
        scoreUI.text = starsCount + "";

        var newGemCount = CurrencyManager.Instance.GetCurrency(CurrencyManager.CURRENCY_TYPE_GEM);
        gemCount = newGemCount;
        gemUI.text = gemCount + "";

        if (GCI != null)
        {
            stage = GCI.Stage;
            stageText.text = "STAGE " + stage;
        } 

        if (GCL != null)
        {
            level = StaticMessage.selectedLevel;
            stageText.text = "LEVEL " + level;
        }

        var newSpeed = GameController.Instance.Avatar.speed;
        speed = newSpeed;
        speedText.text = "SPEED: " + speed;

    }

    public void Animate(Animator animator)
    {
        Debug.Log("Animate");
        animator.SetTrigger("grow");
    }

}
