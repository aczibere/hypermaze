﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class RewardedVideoWindow : MonoBehaviour {

    PopupWindow window;
    public PopupWindow continueWin;
    GameUIManager uiManager = null;

    void Awake()
    {
        window = GetComponent<PopupWindow>();
    }

    public void OKClicked()
    {
        AnalyticsManager.Instance.LogRewardedVideoChoosen(true);
        AdsManager.ShowVideoAd(AdsManager.REWARDED_VIDEO, HandleShowResult);
    }

    public void OnCancelClicked()
    {
        AnalyticsManager.Instance.LogRewardedVideoChoosen(false);
        window.Hide();
        GameController.Instance.Restart();
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");

                CurrencyManager.Instance.AddCurrency(CurrencyManager.CURRENCY_TYPE_GEM, 3);
                if (uiManager == null)
                    uiManager = GameController.Instance.gameUIMan;
                uiManager.Animate(uiManager.animatorGemUI);
                //MessageWindow.Show("Yeah!", "You got 3 gems!");
                continueWin.Show();

                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

        window.Hide();
        GameController.Instance.ReInitLevel();
    }
}
