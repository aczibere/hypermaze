﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUIManager : MonoBehaviour {
    /* MENU INDEXES
    0 - MMRoot;
    1 - DificultySelector;
    2 - LevelSelector;
    3 - Shop
    */
    public SceneLoader sceneloader;
    public LevelSelectorUIManager levelSelectorUIMan;
    public ShopUIManager shopUIManager;
    public GameObject[] uiRoots;
    public BckgSpriteHolder bckgSpriteHolder;
    public AvatarSpriteHolder avatarSpriteHolder;
    int index = 0;

    void Awake()
    {
        Input.multiTouchEnabled = false;
        SetDefaultAvatarSprite();
    }

    void SetDefaultAvatarSprite()
    {
        if (!PlayerPrefs.HasKey(ShopButton.KEY_AVATAR_UNLOCKED))
        {
            var id = avatarSpriteHolder.sprites[0].name;
            Debug.Log("unlocking: " + id);
            PlayerPrefs.SetString(ShopButton.KEY_AVATAR_SELECTED, id);
            PlayerPrefs.SetString(ShopButton.KEY_AVATAR_UNLOCKED, id);
            PlayerPrefs.Save();
        }
    }

    void Start()
    {
        UpdateUI();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            OnBackButtonClicked();
    }

    public void OnLevelsClicked()
    {
        index = 1;
        UpdateUI();
    }

    public void OnDifficultySelected()
    {
        index = 2;
        levelSelectorUIMan.GenerateButtons();
        UpdateUI();
        levelSelectorUIMan.JumpToStartPos();
    }

    public void OnShopSelected()
    {
        index = 3;
        shopUIManager.GenerateButtons();
        StartCoroutine(shopUIManager.JumpToStart());
        UpdateUI();
    }

    public void OnInfiniteClicked()
    {
        bckgSpriteHolder.SetSelectedId("space");
        StaticMessage.isGameInfinite = true;
        sceneloader.LoadScene("game_scene");
    }

    public void OnBackButtonClicked()
    {
        if (index == 0)
        {
            Application.Quit();
        }
        else if (index == 3)// Shop
        {
            index = 0;
        }
        else
        {
            index -= 1;
        }
        
        UpdateUI();
    }

    void UpdateUI()
    {
        for (int i = 0; i < uiRoots.Length; i++)
        {
            uiRoots[i].SetActive( i == index);
        }
    }

    
}
