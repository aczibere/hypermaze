﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MessageWindow : MonoBehaviour {

    public TextMeshProUGUI title;
    public TextMeshProUGUI text;

    [HideInInspector]
    public PopupWindow popup;

    Button OKButton;

    static MessageWindow _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        popup = GetComponent<PopupWindow>();
        OKButton = GetComponentInChildren<Button>();
    }

    public static void Show(string newTitle, string newText, float delay = 0, Action onOK = null)
    {
        _instance.title.text = newTitle;
        _instance.text.text = newText;
        if (delay > 0)
        {

            _instance.StartCoroutine(_instance.popup.ShowDelayed(delay));
        }
        else
        {
            _instance.popup.Show();
        }

        if (onOK != null)
        {
            _instance.OKButton.onClick.RemoveAllListeners();
            _instance.OKButton.onClick.AddListener(() => onOK());
        }
        
    }

}
