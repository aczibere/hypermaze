﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinueGameWindow : MonoBehaviour {

    PopupWindow window;

    void Awake()
    {
        window = GetComponent<PopupWindow>();
    }

    public void OnContinueClicked()
    {
        bool succes = CurrencyManager.Instance.SpendCurreny(CurrencyManager.CURRENCY_TYPE_GEM, 1);
        GameController.Instance.ReInitLevel();
        window.Hide();
    }

    public void OnCancelClicked()
    {
        GameController.Instance.Restart();
        window.Hide();
    }

}
