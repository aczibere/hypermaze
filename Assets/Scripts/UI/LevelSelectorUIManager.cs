﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectorUIManager : MonoBehaviour {

    LevelButton[] buttons;
    public GameObject buttonTemplate;
    public Transform root;
    public ScrollRect scrollRect;
    public AudioSource btnSound;

    public void GenerateButtons()
    {
        buttons = GetComponentsInChildren<LevelButton>(true);
        Debug.Log("level buttons: " + buttons.Length);

        var diff = StaticMessage.selectedDifficulty;
        for (int i = 0; i < diff.numOfLevels; i++)
        {
            LevelButton lvlBtn = null;
            if (buttons == null || i >= buttons.Length)
            {
                var btn = Instantiate(buttonTemplate);
                btn.transform.SetParent(root, false);
                lvlBtn = btn.GetComponent<LevelButton>();
                btn.GetComponent<ButtonSound>().clickSound = btnSound;
            }
            else
            {
                lvlBtn = buttons[i];
            }

            lvlBtn.level = diff.startLevelindex + i;
            if (i == 0)
                LevelsManager.UnlockLevel(lvlBtn.level);
            lvlBtn.UpdateUI();
        }

        if (buttons.Length > diff.numOfLevels)
        {
            for (int i = diff.numOfLevels; i < buttons.Length; i++)
            {
                buttons[i].gameObject.SetActive(false);
            }
        }

    }

    public void JumpToStartPos()
    {
        scrollRect.verticalNormalizedPosition = 1;
    }
}
