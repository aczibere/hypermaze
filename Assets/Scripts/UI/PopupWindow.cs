﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PopupWindow : MonoBehaviour {

    public Dimmer dimmer;

    Animator animator;
    const string visibleState = "popup"; //Modiy 
    const string param = "active";

    bool _active = false;

    static HashSet<GameObject> windows = new HashSet<GameObject>();


    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Show()
    {
        if (Visible())
            return;

        dimmer.Activate();
        windows.Add(gameObject);
        _active = true;
        animator.SetBool(param, _active);
    }

    public void Hide()
    {
        if (!Visible())
            return;

        dimmer.Deactivate();
        _active = false;
        animator.SetBool(param, _active);
    }

    public bool Visible()
    {
        return _active;
    }

    public IEnumerator ShowDelayed(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Show();
    }

    public static bool IsAnyWindoWVisible()
    {
        windows.RemoveWhere(item => item == null);
        foreach (var item in windows)
        { 
            var animator = item.GetComponent<Animator>();
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("base")
                || animator.IsInTransition(0))
            {
                return true;
            }
        }
        return false;
    }

}
