﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelButton : MonoBehaviour {

    public enum State
    {
        Locked,
        Unlocked
    }

    public int level;
    public TextMeshProUGUI text;
    public GameObject lockImage;

    Button btn;
    State state = State.Locked;

    void Awake()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        UpdateState();
        if (state == State.Unlocked)
        {
            StaticMessage.isGameInfinite = false;
            StaticMessage.selectedLevel = level;
            FindObjectOfType<SceneLoader>().LoadScene("game_scene");
        }       
    }

    void UpdateState()
    {
        state = LevelsManager.IsUnlocked(level) ? State.Unlocked : State.Locked;
    }

    public void UpdateUI()
    {
        UpdateState();

        if (state == State.Unlocked)
        {
            text.text = level + "";
            text.gameObject.SetActive(true);
            lockImage.SetActive(false);
        }
        else
        {
            text.gameObject.SetActive(false);
            lockImage.SetActive(true);
        }    
    }



}
