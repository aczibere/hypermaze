﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debugger : MonoBehaviour {

    public bool unlockAllLevels = false;
    public bool deleteCurency = false;
    public bool deleteAllPrefs = false;
    public int levelCount = 200;

	// Use this for initialization
	void Start () {

        Debug.LogError("WARNING: Debugger is still in the scene");

        if (deleteAllPrefs)
        {
            PlayerPrefs.DeleteAll();
            return;
        }

        if (unlockAllLevels)
        {
            for (int i = 0; i < levelCount + 1; i++)
            {
                LevelsManager.UnlockLevel(i);
            }
        }

        if (deleteCurency)
        {
            CurrencyManager.Instance.SetCurrency(CurrencyManager.CURRENCY_TYPE_STAR, 0);
        }

	}
	
}
