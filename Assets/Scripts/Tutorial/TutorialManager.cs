﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {

    public GameObject tutorial;
    public bool deleteTutorialKey = false;
    Timer timer = null;

    public const string KEY_TUTORIAL_SHOWN = "tutorialShown";

    // Use this for initialization
    void Start ()
    {
#if UNITY_EDITOR
        if (deleteTutorialKey)
            PlayerPrefs.DeleteKey(KEY_TUTORIAL_SHOWN);
#endif

        if (TutorialNeeded())
        {
            ShowTutorial();
        }

    }

    public void RestartTimer()
    {
        timer.Restart();
    }

    public void ShowTutorial()
    {
        Debug.Log("Show Tutorial");
        tutorial.SetActive(true);
        SetTutorialNeeded(false);
    }

    public void HideTutorial()
    {
        tutorial.SetActive(false);
    }
    /*
    public void OnToggle()
    {
        SetTutorialNeeded(!TutorialNeeded());
    }
    */
    public static void SetTutorialNeeded(bool needed)
    {
        var val = needed ? 1: 0;
        Debug.Log("SetTutorialNeeded: " + val);
        PlayerPrefs.SetInt(KEY_TUTORIAL_SHOWN, val);
        PlayerPrefs.Save();
    }

    public static bool TutorialNeeded()
    {
        var val = PlayerPrefs.GetInt(KEY_TUTORIAL_SHOWN,1);
        Debug.Log("TutorialNeeded: " + val);
        return  val == 1;
    }

}
