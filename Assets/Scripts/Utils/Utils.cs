﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
public static class Utils  {

    public static void LookAt2D(this Transform original, Transform target)
    {
        original.transform.up = target.position - original.transform.position;
    }

    public static IEnumerator SelfDestruct(this GameObject go, float time)
    {
        yield return new WaitForSeconds(time);

        Debug.Log("Destructing: " + go.name);
        if(go != null)
            GameObject.Destroy(go);
    }

    public static Vector2 GetWorldSize(this RectTransform rect)
    {
        Vector3[] corners = new Vector3[4];
        rect.GetWorldCorners(corners);
        var width = Mathf.Abs(corners[0].x - corners[2].x);
        var height = Mathf.Abs(corners[0].y - corners[1].y);
        return new Vector2(width, height);
    }

    public static IEnumerator TypeText(this Text textComp, string message)
    {
        textComp.text = "";
        foreach (char letter in message.ToCharArray())
        {
            textComp.text += letter;
            yield return null;
        }
    }

    public static IEnumerator MoveFromTo(Transform objectToMove, Vector3 a, Vector3 b, float speed)
    {
        float step = (speed / (a - b).magnitude) * Time.fixedDeltaTime;
        float t = 0;
        while (t <= 1.0f)
        {
            t += step; // Goes from 0 to 1, incrementing by step each time
            objectToMove.position = Vector3.Lerp(a, b, t); // Move objectToMove closer to b
            yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
        }
        objectToMove.position = b;
    }

    public static IEnumerator MoveFromTo(this Transform objectToMove, Vector3 b, float speed, Action onAnimEnnded = null)
    {
        var a = objectToMove.position;
        float step = (speed / (a - b).magnitude) * Time.fixedDeltaTime;
        float t = 0;
        while (t <= 1.0f)
        {
            t += step; // Goes from 0 to 1, incrementing by step each time
            objectToMove.position = Vector3.Lerp(a, b, t); // Move objectToMove closer to b
            yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
        }
        objectToMove.position = b;

        if (onAnimEnnded != null)
            onAnimEnnded();
    }
}
