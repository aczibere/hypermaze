﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Timer : MonoBehaviour {

    public float startTime = 5f;
    public bool startAutomatically = true;
    public Action OnTimerEnded;
    public float TimeLeft
    {
        get { return timeLeft; }
    }

    bool counting = false;
    //TODO: public only for debugging
    public float timeLeft = 0f;

    // Use this for initialization
    void Start()
    {
        if(startAutomatically)
            Restart();
    }

    public void Restart()
    {
        timeLeft = startTime;
        counting = true;
    }

    public void Reset()
    {
        timeLeft = startTime;
        counting = true;
    }

    public void Pause()
    {
        counting = false;
    }

    public void Resume()
    {
        counting = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!counting)
            return;

        //Debug.Log( name + "timeLeft: " + timeLeft);

        timeLeft -= Time.deltaTime;
        if (timeLeft < 0)
        {
            if (OnTimerEnded != null)
            {
                counting = false;
                OnTimerEnded();
            }
        }
    }

    
}
