﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
public class BckgSpriteHolder : MonoBehaviour {

    public const string SELECTED_THEME = "selected_theme";

    public LevelTheme[] levels;

    [Serializable]
    public class LevelTheme
    {
        public string id;
        public Sprite background;
        public Color baseColor;
        public Color wallColor;
        public Color avatarColor;
    }

    static BckgSpriteHolder instance = null;

    public static BckgSpriteHolder Instance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public LevelTheme GetSelectedTheme()
    {
        Debug.Log("GetSelectedTheme: ");
        var id = PlayerPrefs.GetString(SELECTED_THEME, "");
        if (!string.IsNullOrEmpty(id))
        {
            Debug.Log("Loading theme: " + id);

            var level = levels.First(x => x.id == id);
            return level;
        }
        return null;
    }

    public bool SetSelectedId(string id)
    {

        var level = levels.Select(x => x.id == id);
        if (level != null)
        {
            Debug.Log("Saving theme: " + id);
            PlayerPrefs.SetString(SELECTED_THEME, id);
            PlayerPrefs.Save();
            return true;
        }
        else
        {
            return false;
        }
    }
}
