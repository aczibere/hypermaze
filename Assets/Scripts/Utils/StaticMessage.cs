﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticMessage  {

    public static Difficulty selectedDifficulty;
    public static int selectedLevel;
    public static bool isGameInfinite;
}
