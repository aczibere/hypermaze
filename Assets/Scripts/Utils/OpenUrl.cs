﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class OpenUrl : MonoBehaviour {

    public string url;
    Button btn;

	// Use this for initialization
	void Start () {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(OpenURL);
	}

    public void OpenURL()
    {
        Application.OpenURL(url);
    }
}
