﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarSpriteHolder : MonoBehaviour {

    public Sprite[] sprites;

    static AvatarSpriteHolder instance = null;

    public static AvatarSpriteHolder Instance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public Sprite GetSprite(string name)
    {
        foreach (var item in sprites)
        {
            if (item.name == name)
                return item;
        }
        return null;
    }
}
