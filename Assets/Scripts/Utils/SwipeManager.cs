﻿using UnityEngine;
using System;

public enum Swipe { None, Up, Down, Left, Right };

public class SwipeManager : MonoBehaviour
{
    public float minSwipeLength = 5f;
    public float minTime = 0.2f;
    public Action<Swipe> SwipeHappened;

    float elapsedTime = 0;
    bool swipeHappening = false;
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;

    Vector2 firstClickPos;
    Vector2 secondClickPos;

    public static Swipe swipeDirection;

    void Update()
    {
        DetectSwipe();
    }

    void DetectDirection()
    {
        // Make sure it was a legit swipe, not a tap
        if (currentSwipe.magnitude < minSwipeLength)
        {
            swipeDirection = Swipe.None;
            return;
        }

        currentSwipe.Normalize();

        // Swipe up
        if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            swipeDirection = Swipe.Up;
            // Swipe down
        }
        else if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            swipeDirection = Swipe.Down;
            // Swipe left
        }
        else if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            swipeDirection = Swipe.Left;
            // Swipe right
        }
        else if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            swipeDirection = Swipe.Right;
        }

        if (SwipeHappened != null)
        {
            SwipeHappened(swipeDirection);
        }
    }

    void CheckMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstClickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            elapsedTime = 0f;
            swipeHappening = true;
        }
        else
        {
            swipeDirection = Swipe.None;
            //Debug.Log ("None");
        }

        if (elapsedTime > minTime || Input.GetMouseButtonUp(0))
        {
            if (!swipeHappening)
                return;

            swipeHappening = false;
            elapsedTime = 0f;
            secondClickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            currentSwipe = new Vector3(secondClickPos.x - firstClickPos.x, secondClickPos.y - firstClickPos.y);

            //Swipe directional check
            DetectDirection();
        }
    }

    void CheckTouch()
    {
        Touch t = Input.GetTouch(0);

        if (t.phase == TouchPhase.Began)
        {
            firstPressPos = new Vector2(t.position.x, t.position.y);
            elapsedTime = 0f;
            swipeHappening = true;
        }

        if ( t.phase == TouchPhase.Ended || elapsedTime > minTime)
        {

            if (!swipeHappening)
                return;

            swipeHappening = false;
            elapsedTime = 0f;

            secondPressPos = new Vector2(t.position.x, t.position.y);
            currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            DetectDirection();
        }
    }

    public void DetectSwipe()
    {
        if (Input.touches.Length > 0)
        {
            CheckTouch();
        }
        else
        {
            CheckMouse();
        }

        if (swipeHappening)
        {
            elapsedTime += Time.deltaTime;
            //Debug.Log("Elapsedtime: " + elapsedTime);
        }

    }
}