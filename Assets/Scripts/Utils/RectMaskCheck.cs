﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RectMaskCheck : MonoBehaviour {

	// Use this for initialization
	void Awake () {
#if !UNITY_EDITOR
        var rectMask = GetComponent<RectMask2D>();
        rectMask.enabled = true;
#endif
    }

    // Update is called once per frame
    void Update () {
		
	}
}
