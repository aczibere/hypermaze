﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyManager : MonoBehaviour
{
    //TODO: modify theese values to customize
    public const string CURRENCY_TYPE_STAR = "star";
    public const string CURRENCY_TYPE_GEM = "gem";

    const int initialCurreny = 0;

    const string CURRENCY_PREFIX = "currency";

    static CurrencyManager _instance;

    public static CurrencyManager Instance
    {
        get { return _instance; }
    }

    void Awake()
    {

        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        Init();
    }

    void Init()
    {
        //Put init values here
        if (!PlayerPrefs.HasKey(CURRENCY_PREFIX + CURRENCY_TYPE_STAR))
        {
            SetCurrency(CURRENCY_TYPE_STAR, 0);
            SetCurrency(CURRENCY_TYPE_GEM, 3);
        }
    }

    public void SetCurrency(string type, int value)
    {
        Debug.Log("Currency (" + type + ") changed from " + GetCurrency(type) + " to: " + value);
        PlayerPrefs.SetInt(CURRENCY_PREFIX + type, value);
        PlayerPrefs.Save();
    }

    public int GetCurrency(string type, int defaultValue = 0)
    {
        return PlayerPrefs.GetInt(CURRENCY_PREFIX + type, defaultValue);
    }

    public bool SpendCurreny(string type, int value)
    {
        var currentValue = GetCurrency(type, value);
        if (currentValue < value)
            return false;

       SetCurrency(type, currentValue - value);
       return true;
    }

    public void AddCurrency(string type, int value)
    {
        var current = GetCurrency(type);
        var newVal = current + value;
        Debug.Log("Currency (" + type + ") changed from " + current + " to: " + newVal);
        SetCurrency(type, newVal);
    }

    
}
