﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements; // Using the Unity Ads namespace.
using System;

public class AdsManager : MonoBehaviour {

    public const string REWARDED_VIDEO = "rewardedVideo";
    public const string SKIPPABLE_VIDEO = "skippableVid";

    public string gameId; // Set this value from the inspector.
    public bool enableTestMode = true;
    static AdsManager instance;

    public static AdsManager Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    IEnumerator Start()
    {

        if (Advertisement.isSupported)
        { // If runtime platform is supported...
            Advertisement.Initialize(gameId, enableTestMode); // ...initialize.
        }

        Debug.Log("Unity Ads initialized: " + Advertisement.isInitialized);
        Debug.Log("Unity Ads is supported: " + Advertisement.isSupported);
        Debug.Log("Unity Ads test mode enabled: " + Advertisement.testMode);

        // Wait until Unity Ads is initialized,
        //  and the default ad placement is ready. 
        while (!Advertisement.isInitialized || !Advertisement.IsReady())
        {
            yield return new WaitForSeconds(0.5f);
        }
    }

    public static void ShowVideoAd(string id, Action<ShowResult> callback = null)
    {
        if (Advertisement.IsReady(id))
        {
            if (callback == null)
            {
                Advertisement.Show(id);
            }
            else
            {
                var options = new ShowOptions { resultCallback = callback };
                Advertisement.Show(id, options);
            }
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }
}
