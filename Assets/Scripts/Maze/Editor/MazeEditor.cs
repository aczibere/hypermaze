﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MazeSpawner))]
public class MazeEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        MazeSpawner myScript = (MazeSpawner)target;
        if (GUILayout.Button("Generate"))
        {
            myScript.GeneRateNew(Vector3.zero);
        }

    }
}
