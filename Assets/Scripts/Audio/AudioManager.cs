﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour {

    const string KEY_VOLUME = "volume";

    public Image img;
    public Sprite sprOn;
    public Sprite sprOff;

    int volume = 1;

    // Use this for initialization
    void Start ()
    {
        volume = GetVolume();
        AudioListener.volume = volume;
        UpdateUI();
    }

    void UpdateUI()
    {    
        img.sprite = (volume == 1) ? sprOn : sprOff;     
    }

    public void OnAudioButton()
    {
        volume = GetVolume();
        SetVolume(volume == 1 ? 0 : 1);
        UpdateUI();
    }

    int GetVolume()
    {
        return PlayerPrefs.GetInt(KEY_VOLUME, 1);
    }

    void SetVolume(int vol)
    {
        volume = vol;
        AudioListener.volume = volume;
        PlayerPrefs.SetInt(KEY_VOLUME, vol);
        PlayerPrefs.Save();
    }
}
