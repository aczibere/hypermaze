﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSound : MonoBehaviour {

    public AudioSource clickSound;

    void Awake ()
    {
        var btn = GetComponent<Button>();
        if (btn == null)
        {
            Debug.Log("btn is null: " + transform.parent.name);
        }
        btn.onClick.AddListener(PlaySound);      
    }

    void PlaySound()
    {
        clickSound.Play();
    }
	
}
