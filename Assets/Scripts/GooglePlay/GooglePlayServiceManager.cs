﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

public class GooglePlayServiceManager : MonoBehaviour {

    static GooglePlayServiceManager _instance;

    public static GooglePlayServiceManager Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            .Build();

        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        Authenticate();
    }

    public void Authenticate()
    {
        // authenticate user:
        Social.localUser.Authenticate((bool success) => {
            Debug.Log("auth succes: " + success);

        });
    }

    public void ReportScore(int score)
    {
        Social.ReportScore(score, "CgkIzMiBw_UNEAIQAQ", (bool success) => {
            Debug.Log("Score Reported");
        });
    }

    public void ShowLeaderboard()
    {
        //Social.ShowLeaderboardUI();
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkIzMiBw_UNEAIQAQ");
        }
        else
        {
            Authenticate();
        }
    }

}
