﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;


public class GameControllerLevels : GameController
{
    int adCounter = 0;
    public int adFreqency = 9;
    public PopupWindow rateWindow;

    protected override int CalcMazeRowNum()
    {
        if (StaticMessage.selectedDifficulty.id == 0)
            return 6;
        else
            return 8;
    }

    protected override float CalcMazeScale()
    {
        if (StaticMessage.selectedDifficulty.id == 0)
            return 1.3f;
        else
            return 1f;
    }

    protected override float CalcAvatarScale()
    {
        Debug.Log("GameControllerLevels.CalcAvatarScale");
        if (StaticMessage.selectedDifficulty.id == 0)
            return AVATAR_SCALE_BIG;
        else
            return AVATAR_SCALE_SMALL;
    }

    public override void Restart()
    {
        ReInitLevel();
    }

    protected override void MazeSetup()
    {
        goalCounter = StaticMessage.selectedDifficulty.numOfGoals;
        mazeSpawner.numOfGoals = goalCounter;
        avatar.speed = StaticMessage.selectedDifficulty.avatarSpeed;
        mazeSpawner.level = StaticMessage.selectedLevel;
        mazeSpawner.cellmultiplier = CalcMazeScale();
        mazeSpawner.Rows = CalcMazeRowNum();
        mazeSpawner.Columns = CalcMazeRowNum();
    }

    void BackToMainMenu()
    {
        FindObjectOfType<SceneLoader>().LoadScene("mainmenu");
    }

    IEnumerator ShowAdDelayed(string id, float seconds, Action<ShowResult> onVideResult)
    {
        gameState = GameState.WaitingForAd;
        yield return new WaitForSeconds(seconds);
        AdsManager.ShowVideoAd(id, onVideResult);
    }

    private void HandleShowResult(ShowResult result)
    {
        gameState = GameState.ReadyToStart;
        ReInitLevel();
    }

    protected override void OnGameOver(bool won)
    {
        avatar.ShouldMove = false;
        avatar.reactToSwipe = false;
        gameState = GameState.GameEnded;
        avatar.gameObject.SetActive(false);

        adCounter++;

        if (won)
        {
            AnalyticsManager.Instance.LogLevelCompleted(StaticMessage.selectedLevel, StaticMessage.selectedDifficulty.difficultyName);

            var nextLevel = StaticMessage.selectedLevel + 1;
            CurrencyManager.Instance.AddCurrency(CurrencyManager.CURRENCY_TYPE_STAR, StaticMessage.selectedDifficulty.numOfGoals);
            gameUIMan.Animate(gameUIMan.animatorStageText);
            gameUIMan.Animate(gameUIMan.animatorStarUI);
            refHolder.cheers.Play();

            var lastLevel = StaticMessage.selectedDifficulty.startLevelindex + StaticMessage.selectedDifficulty.numOfLevels - 1;
            if (StaticMessage.selectedLevel == lastLevel)
            {
                //var msg = "Congratulations! You unlocked the last " + StaticMessage.selectedDifficulty.difficultyName + " level!";
                //MessageWindow.Show("GAME WON!", msg, 0, BackToMainMenu);
                rateWindow.Show();
            }
            else
            {
                if (adCounter > adFreqency && Advertisement.IsReady(AdsManager.SKIPPABLE_VIDEO))
                {
                    adCounter = 0;
                    StartCoroutine(ShowAdDelayed(AdsManager.SKIPPABLE_VIDEO, 0.2f, HandleShowResult));
                    LevelsManager.UnlockLevel(nextLevel);
                    StaticMessage.selectedLevel = nextLevel;
                    AnimateMaze(onAnimEnd: null);
                    ResetAvatar();
                }
                else
                {
                    LevelsManager.UnlockLevel(nextLevel);
                    StaticMessage.selectedLevel = nextLevel;
                    AnimateMaze(onAnimEnd: ReadyToStart);
                    ResetAvatar();
                }
            }

        }
        else
        {
            if (adCounter > adFreqency && Advertisement.IsReady(AdsManager.SKIPPABLE_VIDEO))
            {
                adCounter = 0;
                StartCoroutine(ShowAdDelayed(AdsManager.SKIPPABLE_VIDEO, 0.3f, HandleShowResult));
            }
            else
            {
                ReInitLevel();
            }
        }
    }

    public override void GoalFound(GameObject goal)
    {
        Debug.Log("GoalFound goalcouter: " + goalCounter + "goal: " + goal.GetInstanceID());
        //CurrencyManager.Instance.AddCurrency(CurrencyManager.CURRENCY_TYPE_STAR, 1);
        base.GoalFound(goal);
    }
}
