﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Touch;
using System;

public abstract class GameController : MonoBehaviour, IGameController {

    public const float AVATAR_SCALE_BIG = 0.15f;
    public const float AVATAR_SCALE_SMALL = 0.12f;

    public enum GameState
    {
        ReadyToStart,
        Playing,
        GameEnded,
        WaitingForAd
    };

    [HideInInspector]
    public GameState gameState = GameState.ReadyToStart;

    [HideInInspector]
    public AvatarSpriteHolder avatarSpriteHolder;

    protected MazeSpawner mazeSpawner;
    protected AvatarController avatar;
    public GameUIManager gameUIMan;
    protected GameControllerRefHolder refHolder;
    protected GameObject currentMaze;
    protected GameObject explosionInstance;

    Vector3 ballStartPos;
    protected int goalCounter = 0;

    static GameController _instance;

    public static GameController Instance
    {
        get { return _instance; }
    }

    public Vector3 BallStartPos
    {
        get { return  ballStartPos; }
    }

    public AvatarController Avatar
    {
        get { return avatar; }
    }

    protected abstract void OnGameOver(bool won);
    protected abstract void MazeSetup();

    protected abstract float CalcAvatarScale();
    protected abstract float CalcMazeScale();
    protected abstract int CalcMazeRowNum();

    public abstract void Restart();

    // Use this for initialization
    void Awake () {

        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

            SetupReferences();

        ballStartPos = avatar.transform.position;
        gameState = GameState.ReadyToStart;
    }

    void Start()
    {       
        gameUIMan.UpdateUI();
        Restart();
    }

    protected virtual void OnEnable()
    {
        // Hook into the events we need
        LeanTouch.OnFingerSwipe += OnFingerSwipe;
    }

    protected virtual void OnDisable()
    {
        // Unhook the events
        LeanTouch.OnFingerSwipe -= OnFingerSwipe;
    }

    void SetupReferences()
    {
        mazeSpawner = FindObjectOfType<MazeSpawner>();
        avatar = FindObjectOfType<AvatarController>();
        avatarSpriteHolder = FindObjectOfType<AvatarSpriteHolder>();
        gameUIMan = GetComponent<GameUIManager>();
        refHolder = GetComponent<GameControllerRefHolder>();
    }

    public void OnWallHit(Vector3 pos)
    {
        refHolder.audioCollision.Play();
        OnGameOver(false);

        if (explosionInstance == null)
        {
            explosionInstance = Instantiate(refHolder.explosion, pos, Quaternion.identity);
        }
        else
        {
            explosionInstance.transform.position = pos;
        }

        var col = avatar.baseSprite.color;
        explosionInstance.GetComponent<AvatarExplosion>().Explode(col);
        //StartCoroutine(explosionInst.SelfDestruct(2));

    }

    public virtual void GoalFound(GameObject goal)
    {
        refHolder.audioStarPuckUp.Play();

        //TODO: kill particles
        var sp = Instantiate(refHolder.starParticle, goal.transform.position, Quaternion.identity);
        StartCoroutine(sp.SelfDestruct(2));

        Destroy(goal);
        goalCounter--;


        if (goalCounter <= 0)
        {
            OnGameOver(true);
        }

        gameUIMan.UpdateUI();
    }

    public void OnMazeTrigger()
    {
        mazeSpawner.firstWall.SetActive(true);
        mazeSpawner.firstWall.GetComponent<SpriteRenderer>().color = new Color(1,1,1,0);
    }

    public void ReInitLevel()
    {
        Debug.Log("ReinitLevel");
        //mazeSpawner.DestroyMaze();
        if (currentMaze != null)
            Destroy(currentMaze);

        MazeSetup();
        currentMaze = mazeSpawner.GeneRateNew(Vector3.zero);

        ResetAvatar();
        StartCoroutine(SetGameStateDelayed(GameState.ReadyToStart, 0.2f));
    }

    public IEnumerator SetGameStateDelayed(GameState gState, float delay)
    {
        yield return new WaitForSeconds(delay);
        gameState = gState;
    }

    protected void ResetAvatar()
    {
        Debug.Log("ResetAvatar");

        avatar.transform.position = new Vector3(mazeSpawner.GetAvatarPosX(), ballStartPos.y,ballStartPos.z);  //ballStartPos;
        avatar.gameObject.SetActive(true);
        avatar.transform.localScale = CalcAvatarScale() * Vector3.one;

        goalCounter = mazeSpawner.numOfGoals;
        //startButton.gameObject.SetActive(true);

        gameUIMan.UpdateUI();
        gameUIMan.startArrow.SetActive(true);
    }

    public void StartAvatar()
    {
        Debug.Log("StartClicked");
        gameState = GameState.Playing;
        avatar.reactToSwipe = false;
        avatar.ShouldMove = true;
        avatar.dir = Vector2.up;
        //startButton.gameObject.SetActive(false);
        gameUIMan.startArrow.SetActive(false);
        gameUIMan.tutManager.HideTutorial();
    }

    public void OnFingerSwipe(LeanFinger finger)
    {
        Debug.Log("OnFingerSwipe");
        var winVis = PopupWindow.IsAnyWindoWVisible();
        if (gameState == GameState.ReadyToStart && !winVis)
        {
            var swipe = finger.SwipeScreenDelta;
            if (swipe.y > Mathf.Abs(swipe.x) && Mathf.Abs(swipe.y) > 100) // && Mathf.Abs(swipe.y) > 150
            {
                //Swipe up
                StartAvatar();
            }
        }
    }

    protected void ReadyToStart()
    {

        gameState = GameState.ReadyToStart;
        gameUIMan.startArrow.SetActive(true);
    }

    protected void AnimateMaze(Action onAnimEnd )
    {
        MazeSetup();
        var newMaze = mazeSpawner.GeneRateNew(new Vector3(10, 0, 0));
        StartCoroutine(currentMaze.transform.MoveFromTo(new Vector3(-10, 0, 0), 20));
        StartCoroutine(newMaze.transform.MoveFromTo(new Vector3(0, 0, 0), 20, onAnimEnd));
        StartCoroutine(currentMaze.SelfDestruct(2));
        currentMaze = newMaze;
    }
}
