﻿using Lean.Touch;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AvatarController : MonoBehaviour
{
    public float speed = 1;
    public SpriteRenderer avatarIcon;

    [Header("Audio")]
    public AudioSource[] directionChangeAudios;

    ParticleSystem trailParticles;
    bool shouldMove = false;

    [HideInInspector]
    public Vector3 dir = Vector3.forward;
    [HideInInspector]
    public SpriteRenderer baseSprite;
    [HideInInspector]
    public bool reactToSwipe = false;

    public bool ShouldMove
    {
        set { shouldMove = value; SetParticleState(shouldMove); }
        get { return shouldMove; }
    }

    void Awake()
    {
        trailParticles = GetComponent<ParticleSystem>();
        baseSprite = GetComponentInChildren<SpriteRenderer>();
        SetupParticles();
    }

    void Start()
    {
        var srName = PlayerPrefs.GetString(ShopButton.KEY_AVATAR_SELECTED);
        Debug.Log("avatar sprite: " + srName);
        if(!string.IsNullOrEmpty(srName))
            avatarIcon.sprite = GameController.Instance.avatarSpriteHolder.GetSprite(srName);
    }

    // Update is called once per frame
    void Update()
    {
        if (!shouldMove)
            return;
        transform.position += dir.normalized * speed * Time.deltaTime;
    }

    void PlayDirChangeAudio()
    {
        var index = Random.Range(0, directionChangeAudios.Length);
        directionChangeAudios[index].Play();
    }

    protected virtual void OnEnable()
    {
        // Hook into the events we need
        LeanTouch.OnFingerSwipe += OnFingerSwipe;
    }

    protected virtual void OnDisable()
    {
        // Unhook the events
        LeanTouch.OnFingerSwipe -= OnFingerSwipe;
    }

    void SetParticleState(bool enable)
    {
        if (enable)
        {
            trailParticles.Play();
        }
        else
        {
            trailParticles.Stop();
            trailParticles.Clear();
        }

    }

    public void ChangeColor(Color col)
    {
        baseSprite.color = col;
        SetupParticles();
    }

    void SetupParticles()
    {
        var col = trailParticles.colorOverLifetime;
        Gradient grad = new Gradient();
        grad.SetKeys(new GradientColorKey[] { new GradientColorKey(baseSprite.color, 0.0f), new GradientColorKey(baseSprite.color, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(0.0f, 1.0f) });
        col.color = grad;
    }
    
    public void OnFingerSwipe(LeanFinger finger)
    {
        if (!reactToSwipe)
            return;

        PlayDirChangeAudio();

        // Store the swipe delta in a temp variable
        var swipe = finger.SwipeScreenDelta;

        if (swipe.x < -Mathf.Abs(swipe.y))
        {
            //InfoText.text = "You swiped left!";
            dir = Vector2.left;
        }

        if (swipe.x > Mathf.Abs(swipe.y))
        {
            dir = Vector2.right;
        }

        if (swipe.y < -Mathf.Abs(swipe.x))
        {
            dir = Vector2.down;
        }

        if (swipe.y > Mathf.Abs(swipe.x))
        {
             dir = Vector2.up;
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Wall")
        {
            GameController.Instance.OnWallHit(transform.position);
        }
        else if (other.gameObject.tag == "Goal")
        {
            GameController.Instance.GoalFound(other.gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "MazeTrigger")
        {
            reactToSwipe = true;
            GameController.Instance.OnMazeTrigger();

            Destroy(other.gameObject);
        }
    }
}

