﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class GameControllerInfinite : GameController {

    int _stage = 0;
    float startSpeed = 1.6f;

    public int Stage
    {
        get { return _stage; }
    }

    public override void GoalFound(GameObject goal)
    {
        CurrencyManager.Instance.AddCurrency(CurrencyManager.CURRENCY_TYPE_STAR,  1); //_stage +
        gameUIMan.Animate(gameUIMan.animatorStarUI);
        base.GoalFound(goal);
    }

    protected override void OnGameOver(bool won)
    {
        avatar.ShouldMove = false;
        avatar.reactToSwipe = false;
        gameState = GameState.GameEnded;
        

        avatar.gameObject.SetActive(false);
        if (won)
        {
            AnalyticsManager.Instance.LogInfiniteCompleted(_stage);      
            StageCompleted();
        }
        else
        {
            if (_stage == 0)
            {
                Restart();
            }
            else
            {
                if (CurrencyManager.Instance.GetCurrency(CurrencyManager.CURRENCY_TYPE_GEM) > 0)
                {
                    //TODO: reference PopUpWindow instead
                    gameUIMan.contGameWin.GetComponent<PopupWindow>().Show();
                }
                else
                {
                    if (Advertisement.IsReady(AdsManager.REWARDED_VIDEO))
                    {
                        //TODO: reference PopUpWindow instead
                        gameUIMan.rewVideoWin.GetComponent<PopupWindow>().Show();
                    }
                    else
                    {
                        Restart();
                    }

                }
            }
        }
    }

    void StageCompleted()
    {
        Debug.Log("StageCompleted");
        refHolder.cheers.Play();
        GooglePlayServiceManager.Instance.ReportScore(_stage);
        //var msg = "You have succesfully passed stage " + _stage + "!";
        //MessageWindow.Show("Congratulations!", msg, 0.35f, ReInitLevel);
        _stage++;
        gameUIMan.Animate(gameUIMan.animatorStageText);
        gameUIMan.Animate(gameUIMan.animatorSpeedText);

        AnimateMaze(ReadyToStart);
        ResetAvatar();
    }

    protected override float CalcAvatarScale()
    {
        if (_stage < 4)
            return AVATAR_SCALE_BIG;
        else
            return AVATAR_SCALE_SMALL;
    }

    protected override float CalcMazeScale()
    {
        if (_stage < 4)
            return 1.3f;
        else
            return 1f;
    }

    protected override int CalcMazeRowNum()
    {
        if (_stage < 4)
            return 6;
        else
            return 8;
    }

    float CalcSpeed()
    {
        float speed = startSpeed;
        if (_stage < 4)
        {
            speed = startSpeed + 0.1f * _stage;
        }
        else
        {
            speed = startSpeed + 0.1f * (_stage -4 );
        }
        Debug.Log("Calc speed: " + speed);
        return speed;
    }

    protected override void MazeSetup()
    {
        goalCounter = 1;
        mazeSpawner.numOfGoals = goalCounter;
        avatar.speed = CalcSpeed();
        mazeSpawner.level = 0; // 0 means random levels
        mazeSpawner.cellmultiplier = CalcMazeScale();
        mazeSpawner.Rows = CalcMazeRowNum();
        mazeSpawner.Columns = CalcMazeRowNum();

        gameUIMan.UpdateUI();
    }

    public override void Restart()
    {
        _stage = 0;
        ReInitLevel();
    }

}
