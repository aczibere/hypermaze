﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControllerRefHolder : MonoBehaviour {

    [Header("REFERENCES")]
    public GameObject explosion;
    public GameObject starParticle;
    public PopupWindow rateWindow;

    [Header("AUDIO REFERENCES")]
    public AudioSource cheers;
    public AudioSource audioStarPuckUp;
    public AudioSource audioCollision;

    [Header("DEBUG")]
    public Difficulty debugDiff;

    void Awake()
    {

#if UNITY_EDITOR
        if (StaticMessage.selectedDifficulty == null)
        {
            StaticMessage.selectedLevel = 1;
            StaticMessage.selectedDifficulty = debugDiff;
        }
            
#endif

        if (StaticMessage.isGameInfinite)
        {
            AnalyticsManager.Instance.LogGameModeSelected("infinite");
            var script = gameObject.AddComponent<GameControllerInfinite>();
            GetComponent<GameUIManager>().GCI = script;
        }
        else
        {
            AnalyticsManager.Instance.LogGameModeSelected("levels");
            var script = gameObject.AddComponent<GameControllerLevels>();
            script.rateWindow = rateWindow;
            GetComponent<GameUIManager>().GCL = script;
        }
    }

}
