﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager : MonoBehaviour {

    static AnalyticsManager instance;

    public static AnalyticsManager Instance
    {
        get { return instance; }
    }

    Dictionary<string, object> parameters = new Dictionary<string, object>();

    void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public void LogInfiniteCompleted(int stage)
    {
        parameters.Clear();
        parameters["stage"] = "stage" + stage;
        Analytics.CustomEvent("infinite_completed", parameters);
    }

    public void LogLevelCompleted(int level, string difficulty)
    {
        parameters.Clear();
        parameters["level"] = "level" + level;
        parameters["difficulty"] = difficulty;
        Analytics.CustomEvent("level_completed", parameters);
    }

    public void LogRewardedVideoChoosen(bool videoChosen)
    {
        parameters.Clear();
        parameters["video_chosen"] = videoChosen;
        Analytics.CustomEvent("rewarded_video", parameters);
    }

    public void LogGameModeSelected(string gameMode)
    {
        parameters.Clear();
        parameters["gamemode"] = gameMode;
        Analytics.CustomEvent("gamemode_selected", parameters);
    }

    public void LogAvatarBought(int price)
    {
        parameters.Clear();
        parameters["price"] = price;
        Analytics.CustomEvent("avatar_bought", parameters);
    }
}
