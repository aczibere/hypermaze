﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarExplosion : MonoBehaviour {

    ParticleSystem particles;

	void Awake () {
        particles = GetComponent<ParticleSystem>();
    }

    public void Explode(Color color)
    {
        particles.Stop();
        particles.Clear();
        particles.startColor = color;
        particles.Play();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
